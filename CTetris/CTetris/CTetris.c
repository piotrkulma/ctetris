﻿#include "Element.h"
#include "GameModel.h"

void drawUI();

void drawUI()
{
	for (int i = 0; i <= 20; i++)
	{
		gotoxy(i, 20);
		putch('*');
	}
}

int main()
{
	int timer = 0;
	GameModelPtr gameModel = newGameModel();

	ElementPtr element = malloc(sizeof(Element) * 1);
	element->mainElementIndex = 1;
	element->subElementIndex = 1;
	element->position.X = 5;
	element->position.Y = 17;

	gameModel->previousElements->element = element;

	drawUI();

	ElementListPtr previousElements = malloc(sizeof(ElementList) * 1);
	previousElements->element = &element;

	while (gameModel->game)
	{
		if (kbhit())
		{
			int keyCode = getch();
			updateGameModelOnKeyAction(gameModel, keyCode);
		}

		if (timer >= 80 && gameModel->pause == FALSE) {
			showElement(*(gameModel->element), FALSE);
			showElements(gameModel->previousElements, FALSE);

			if (checkCollisionBetweenElementList(*(gameModel->element), gameModel->previousElements))
			{
				storeElement(gameModel);
				createNewElement(gameModel);
			}
			moveDownElement(gameModel->element);

			showElement(*(gameModel->element), TRUE);
			showElements(gameModel->previousElements);

			timer = 0;
		}

		timer++;
		Sleep(10);
	}

    return 0;
}