#include "Element.h"

char ELEMENTS_ARRAY[][5][9] =
{
	//BRICK ELEMENT
	{
		{
			'1', '1', '1',
			'1', '1', '1',
			'1', '1', '1',
		},
		{ NULL }
	},

	//L ELEMENT
	{
		{
			'0', '1', '0',
			'0', '1', '0',
			'0', '1', '1',
		},
		{
			'0', '0', '1',
			'1', '1', '1',
			'0', '0', '0',
		},
		{
			'1', '1', '0',
			'0', '1', '0',
			'0', '1', '0',
		},
		{
			'0', '0', '0',
			'1', '1', '1',
			'1', '0', '0',
		},
		{ NULL }
	},

	//I ELEMENT
	{
		{
			'0', '1', '0',
			'0', '1', '0',
			'0', '1', '0',
		},
		{
			'0', '0', '0',
			'1', '1', '1',
			'0', '0', '0',
		},
		{
			'0', '1', '0',
			'0', '1', '0',
			'0', '1', '0',
		},
		{
			'0', '0', '0',
			'1', '1', '1',
			'0', '0', '0',
		},
		{ NULL }
	},
	//T ELEMENT
	{
		{
			'1', '1', '1',
			'0', '1', '0',
			'0', '1', '0',
		},
		{
			'1', '0', '0',
			'1', '1', '1',
			'1', '0', '0',
		},
		{
			'0', '1', '0',
			'0', '1', '0',
			'1', '1', '1',
		},
		{
			'0', '0', '1',
			'1', '1', '1',
			'0', '0', '1',
		},
		{ NULL }
	},
	//H ELEMENT
	{
		{
			'1', '0', '1',
			'1', '1', '1',
			'1', '0', '1',
		},
		{
			'1', '1', '1',
			'0', '1', '0',
			'1', '1', '1',
		},
		{ NULL }
	},
	{ NULL }
};

void showElement(Element element, int show)
{
	if (show) {
		showElementOnScreen(element, FALSE, ELEMENT_FILLED_CHARACTER, ELEMENT_BLANK_CHARACTER);
	}
	else
	{
		showElementOnScreen(element, FALSE, ELEMENT_BLANK_CHARACTER, ELEMENT_BLANK_CHARACTER);
	}
}

void showElements(ElementListPtr elements)
{
	ElementListPtr head = elements;

	if (head->element != NULL)
	{
		showElementOnScreen(*(head->element), TRUE, ELEMENT_FILLED_CHARACTER, ELEMENT_BLANK_CHARACTER);
	}

	while (head->next != NULL)
	{
		head = head->next;
		showElementOnScreen(*(head->element), TRUE, ELEMENT_FILLED_CHARACTER, ELEMENT_BLANK_CHARACTER);
	}
}

void showElementOnScreen(Element element, int ignioreBlank, char filled, char empty)
{
	COORD localCoordinates = element.position;

	for (int characterIndex = 0; characterIndex < ELEMENT_CHARACTERS; characterIndex++) {
		if (characterIndex % ELEMENT_COLS == 0) {
			localCoordinates.X = element.position.X;
			localCoordinates.Y++;
		}
		gotoxy(localCoordinates.X, localCoordinates.Y);
		char actualElement = ELEMENTS_ARRAY[element.mainElementIndex][element.subElementIndex][characterIndex];

		if (actualElement == ELEMENT_FILLED_FIELD)
		{
			putch(filled);
		} 
		else if(!ignioreBlank)
		{
			putch(empty);
		}

		localCoordinates.X++;
	}
}

void changeSubElement(ElementPtr element)
{
	if (ELEMENTS_ARRAY[element->mainElementIndex][(element->subElementIndex + 1)][0] != NULL)
	{
		element->subElementIndex++;
	}
	else {
		element->subElementIndex = 0;
	}
}

void changeMainElement(ElementPtr element)
{
	element->subElementIndex = 0;
	if (ELEMENTS_ARRAY[element->mainElementIndex + 1][(element->subElementIndex)][0] != NULL)
	{
		element->mainElementIndex++;
	}
	else {
		element->mainElementIndex = 0;
	}
}

void gotoxy(int x, int y)
{
	COORD pos = { x, y };
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
}

COORD* getElementCoordinates(Element e)
{
	COORD* coords = malloc(sizeof(COORD) * 9);

	int x = e.position.X, y = e.position.Y;

	for (int i = 0; i < 9; i++)
	{
		if (i % 3 == 0)
		{
			x = e.position.X;
			y++;
		}

		x++;

		if (ELEMENTS_ARRAY[e.mainElementIndex][e.subElementIndex][i] == ELEMENT_FILLED_FIELD)
		{
			COORD c = { x, y };
			coords[i] = c;
		}
		else
		{
			COORD c = { -1, -1 };
			coords[i] = c;
		}
	}

	return coords;
}

int checkCollisionBetweenElements(Element e1, Element e2)
{
	COORD* coords1 = getElementCoordinates(e1);
	COORD* coords2 = getElementCoordinates(e2);

	for (int i = 0; i<9; i++)
	{
		for (int j = 0; j<9; j++)
		{
			if (i != j 
				&& coords1[i].X != -1 
				&& coords1[i].Y != -1
				&& coords2[j].X != -1 
				&& coords2[j].Y != -1
				&& coords1[i].X == coords2[j].X
				&& coords1[i].Y == coords2[j].Y)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

int checkCollisionBetweenElementList(Element e, ElementListPtr list)
{
	e.position.Y++;
	ElementListPtr head = list;

	if (head->element != NULL)
	{
		if (checkCollisionBetweenElements(e, *head->element))
		{
			return TRUE;
		}
	}

	while (head->next != NULL)
	{
		head = head->next;

		if (checkCollisionBetweenElements(e, *head->element))
		{
			return TRUE;
		}
	}

	return FALSE;
}

void moveDownElement(ElementPtr element)
{
	element->position.Y++;
}
