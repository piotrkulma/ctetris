#pragma once

#include <stdio.h>
#include <Windows.h>

#define ELEMENT_BRICK 0
#define ELEMENT_L 1
#define ELEMENT_I 2
#define ELEMENT_T 3
#define ELEMENT_H 4

#define ELEMENT_COLS 3
#define ELEMENT_CHARACTERS ELEMENT_COLS*ELEMENT_COLS

#define ELEMENT_BLANK_FIELD '0'
#define ELEMENT_FILLED_FIELD '1'

#define ELEMENT_FILLED_CHARACTER '*'
#define ELEMENT_BLANK_CHARACTER ' '

typedef struct _element
{
	COORD position;
	int mainElementIndex;
	int subElementIndex;
} Element;

typedef Element* ElementPtr;

typedef struct _elements_list
{
	ElementPtr element;
	struct _elements_list* next;
} ElementList;

typedef ElementList* ElementListPtr;

char ELEMENTS_ARRAY[][5][9];

void showElement(Element element, int show);
void showElements(ElementListPtr elements);
void showElementOnScreen(Element element, int ignoreBlank, char filled, char empty);

void changeSubElement(ElementPtr element);
void changeMainElement(ElementPtr element);

void gotoxy(int x, int y);

COORD* getElementCoordinates(Element e);
int checkCollisionBetweenElements(Element e1, Element e2);
int checkCollisionBetweenElementList(Element e, ElementListPtr list);

void moveDownElement(ElementPtr element);