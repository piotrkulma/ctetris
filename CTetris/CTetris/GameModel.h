#pragma once

#include "Element.h"

#define TRUE 1
#define FALSE 0

#define KEY_ARROW_UP 72
#define KEY_ARROW_RIGHT 77
#define KEY_ARROW_DOWN 80
#define KEY_ARROW_LEFT 75
#define KEY_ESC 27
#define KEY_ENTER 13

typedef struct _game_model
{
	int game;
	int pause;
	ElementPtr element;
	ElementListPtr previousElements;
} GameModel;

typedef GameModel* GameModelPtr;

GameModelPtr newGameModel();
void storeElement(GameModelPtr gameModel);
void createNewElement(GameModelPtr gameModel);
void updateGameModelOnKeyAction(GameModelPtr gameModel, int keyCode);
