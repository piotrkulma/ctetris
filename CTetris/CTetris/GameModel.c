#include "GameModel.h"
#include "Element.h"

GameModelPtr newGameModel()
{
	GameModelPtr gameModel = malloc(sizeof(GameModel) * 1);

	gameModel->pause = FALSE;
	gameModel->game = TRUE;

	createNewElement(gameModel);

	gameModel->previousElements = malloc(sizeof(ElementList) * 1);
	gameModel->previousElements->next = NULL;
	gameModel->previousElements->element = NULL;

	return gameModel;
}

void storeElement(GameModelPtr gameModel)
{
	ElementListPtr head = gameModel->previousElements;

	while (head->next != NULL) {
		head = head->next;
	}

	head->next = malloc(sizeof(ElementList) * 1);
	head = head->next;
	head->element = gameModel->element;
	head->next = NULL;
}

void createNewElement(GameModelPtr gameModel)
{
	ElementPtr element = malloc(sizeof(Element) * 1);
	element->mainElementIndex = 1;
	element->subElementIndex = 1;
	element->position.X = 6;
	element->position.Y = 0;

	gameModel->element = element;
}

void updateGameModelOnKeyAction(GameModelPtr gameModel, int keyCode)
{
	showElement(*(gameModel->element), FALSE);

	if (keyCode == KEY_ENTER)
	{
		changeMainElement(gameModel->element);
	}
	else if (keyCode == KEY_ARROW_UP)
	{
		changeSubElement(gameModel->element);
	}
	else if (keyCode == KEY_ARROW_DOWN)
	{
		if (!checkCollisionBetweenElementList(*(gameModel->element), gameModel->previousElements))
		{
			gameModel->element->position.Y++;
		}
	}
	else if (keyCode == KEY_ARROW_LEFT)
	{
		gameModel->element->position.X--;
	}
	else if (keyCode == KEY_ARROW_RIGHT)
	{
		gameModel->element->position.X++;
	}
	else if (keyCode == KEY_ESC)
	{
		gameModel->game = FALSE;
	}

	showElement(*(gameModel->element), TRUE);
}